import React from 'react';
import { Link } from 'react-router-dom';
import { Menu } from 'semantic-ui-react';

import GoogleAuth from './GoogleAuth';

const Header = () => {
  return (
    <Menu>
      <Menu.Item
      >
        <Link to="/">Streamy</Link>
      </Menu.Item>

      <Menu.Item position="right"
      >
        <Link to="/">All streams</Link>
      </Menu.Item>
      <Menu.Item><GoogleAuth></GoogleAuth></Menu.Item>
    </Menu>
  )
};

export default Header;
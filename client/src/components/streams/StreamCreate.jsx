import React from 'react';
import { connect } from 'react-redux';
import { 
  Container,
} from 'semantic-ui-react';
import { createStream } from '../../store/actions';
import StreamForm from './StreamForm';


const StreamCreate = ({createStream}) => {

  const onSubmit = (formValues) => {
    createStream(formValues)
  };

  return (
  <Container>
    <h3>Create a stream</h3>
    <StreamForm onSubmit={onSubmit} />
  </Container>
  );
};

export default connect(null, { createStream })(StreamCreate);
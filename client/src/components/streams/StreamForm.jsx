import React from 'react';
import {
  Field,
  reduxForm
} from 'redux-form';
import { 
  Button,
  Container,
  Input,
  Form,
  Label,
} from 'semantic-ui-react';

const renderError = ({error, touched}) => {
  if (touched && error) {
    return <Label basic color='red' pointing>{error}</Label>
  }
};

const renderInput = ({input, label, meta}) => {
  return (
    <Form.Field>
      <Label pointing="below" >{label}</Label>
      <Input {...input}/>
      {renderError(meta)}
    </Form.Field>
  );
};

const StreamForm = ({handleSubmit, onSubmit}) => {

  const onFormSubmit = (formValues) => {
    onSubmit(formValues)
  };

  return (
  <Container>
    <Form onSubmit={handleSubmit(onFormSubmit)}>
      <Field name="title" component={renderInput} label="Enter Title"></Field>
      <Field name="description" component={renderInput} label="Enter Description"></Field>
      <Button type="submit" basic color="blue">Submit</Button>
    </Form>
  </Container>
  );
};

const validate = (formValues) => {
  const errors = {};
  if(!formValues.title) {
    errors.title = 'enter a title';
  }
  if (!formValues.description) {
    errors.description = 'enter a description';
  }
  return errors;
};

export default reduxForm({
  form: 'streamForm',
  validate,
})(StreamForm);


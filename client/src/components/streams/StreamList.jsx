import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  Button,
  Icon,
  Container,
  List
} from 'semantic-ui-react';
import { fetchStreams } from '../../store/actions';

const StreamList = ({ 
  streams,
  fetchStreams,
  currentUserId,
  isSignedIn,
  }) => {
  useEffect(() => {
    fetchStreams();
  }, []);

  const renderCreate = () => {
    if (isSignedIn) {
      return (
        <div>
          <Link to="/streams/new"><Button color="teal"><Icon name="plus"/>New</Button></Link>
        </div>
      )
    }
  };

  const renderAdmin = (stream) => {
    if (stream.userId === currentUserId) {
      return (
        <List.Content floated="right">
          <Link to={`/streams/edit/${stream.id}`}>
            <Button
              basic
              color="teal"
              size="small"
            >Edit</Button>
          </Link>
          <Link to={`/streams/delete/${stream.id}`}>
            <Button
              basic
              color="red"
              size="small"
            >Delete</Button>
          </Link>
        </List.Content>
      )
    }
  };

  const renderList = () => {
    return streams.map(stream => {
      return (
        <List.Item key={stream.id}>
          {renderAdmin(stream)}
          <List.Icon
            name="camera retro"
            size='large'
            verticalAlign='middle'
          ></List.Icon>
          <List.Content>
            <List.Header>{stream.title}</List.Header>
            <List.Description>{stream.description}</List.Description>
          </List.Content>
        </List.Item>
      );
    });
  };

  return (
    <Container>
      <h2>Streams</h2>
      <List
        divided
        size="big"
      >{renderList()}</List>
      {renderCreate()}
    </Container>
  );
};

const mapStateToProps = state => {
  return {
    streams: Object.values(state.streams),
    currentUserId: state.auth.userId,
    isSignedIn: state.auth.isSignedIn,
  };
};

export default connect(mapStateToProps, { fetchStreams })(StreamList);
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Container } from 'semantic-ui-react';
import _, { pick } from 'lodash';
import { fetchStream, editStream } from '../../store/actions';
import StreamForm from './StreamForm';

const StreamEdit = ({match, stream, fetchStream, editStream}) => {
  useEffect(() => {
    fetchStream(match.params.id)
  }, []);

  const onSubmit = (formValues) => {
    editStream(match.params.id, formValues);
  }

  return (
    <Container>
      <h3>Edit a stream</h3>
      <div>{stream ? <StreamForm onSubmit={onSubmit} initialValues={_.pick(stream, 'title', 'description')}/> : <div>Loading...</div>}</div>
    </Container>
  )
};

const mapStateToProps = (state, ownProps) => {
  return {
    stream: state.streams[ownProps.match.params.id]
  };
};

export default connect(mapStateToProps, {
  fetchStream,
  editStream,
})(StreamEdit);
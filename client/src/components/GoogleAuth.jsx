import React, { useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import { Button, Icon } from 'semantic-ui-react';
import { signIn, signOut } from '../store/actions';
 
function GoogleAuth ({isSignedIn, signIn, signOut}) {
  const auth = useRef('');
 
  useEffect(() => {
    window.gapi.load('client:auth2', () => {
      window.gapi.client
        .init({
          clientId:
            '564507930030-hkv35kvem2r4ra3bikh61mad7j3k9p85.apps.googleusercontent.com',
          scope: 'email',
        })
        .then(() => {
          auth.current = window.gapi.auth2.getAuthInstance();
          // setIsSignedIn(auth.current.isSignedIn.get());
          onAuthChange(auth.current.isSignedIn.get())
          auth.current.isSignedIn.listen(onAuthChange);
        });
    });
  }, []);

  const onAuthChange = isSignedIn => {
    if (isSignedIn) {
      signIn(auth.current.currentUser.get().getId());
    } else {
      signOut();
    }
  };

  const onSignIn = () => {
    auth.current.signIn();
  };
 
  const onSignOut = () => {
    auth.current.signOut();
  };
 
  const renderAuthButton = () => {
    if (isSignedIn === null) {
      return null;
    } else if (isSignedIn) {
      return (
        <Button
          onClick={onSignOut}
          color="google plus"
        >
          <Icon name="google" />Sign Out
        </Button>
      );
    } else {
      return (
        <Button
          onClick={onSignIn}
          color="google plus"
        >
          <Icon name="google" />Sign In with Google
        </Button>
      );
    }
  };
 
  return <div>{renderAuthButton()}</div>;
}

const mapStateToProps = (state) => {
  return {
    isSignedIn: state.auth.isSignedIn,
  }
}
 
export default connect(
  mapStateToProps,
  { signIn, signOut}
)(GoogleAuth);
